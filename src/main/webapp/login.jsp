<%@page import="java.util.Date"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8" %>
<%@page errorPage="erro.jsp" isErrorPage="false" %>

<jsp:useBean class="pratica.jsp.LoginBean" id="info" scope="session"/>
<jsp:setProperty name="info" property="*" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Login</title>
   </head>
   <body>
      <form method="post" action="/pratica-jsp/login.jsp">
         Código: <input type="text" name="login"/><br/>
         Nome: <input type="password" name="senha"/><br/>
         Perfil: <select name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/>
         <%
             if (request.getMethod().equals("POST")) {        
                String login = request.getParameter("login");
                String senha = request.getParameter("senha");
                
                if(!login.equals(senha)){
                    %>
                    <div>
                        <font color="red"><i>Acesso negado</i></font>
                    </div>
                    <%
                }else{
                    %>
                    <div>
                        <font color="blue">
                            <%
                                String perfil;
                                switch(Integer.parseInt(request.getParameter("perfil"))){
                                    case 1:
                                        perfil = "Cliente";
                                        break;
                                    case 2:
                                        perfil = "Gerente";
                                        break;
                                    case 3:
                                        perfil = "Administrador";
                                        break;
                                    default:
                                        perfil = "Unknow";
                                }
                                
                                Date data = new Date();
                            %>
                            <%=perfil%>, login bem sucedido, para <%=login%> às <%=data.toString()%> <%
                            %>
                        </font>
                    </div>
                    <%
                }
             } 
             
         %>
      </form>
   </body>
</html>

                    
<!--% 
    if (request.getMethod().equals("POST")) {        
        response.sendRedirect(request.getContextPath() + "/sucesso.jsp");
    } 
%-->
